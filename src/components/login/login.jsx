
import React from 'react';
import { Paper, withStyles, Grid, TextField, Button } from '@material-ui/core';
import { Face, Fingerprint } from '@material-ui/icons'

import ApiService from "../../config/services/apiService";

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const styles = theme => ({
    margin: {
        margin: theme.spacing.unit * 2,
    },
    padding: {
        padding: theme.spacing.unit
    }
});

class LoginTab extends React.Component {


  
    MySwal = withReactContent(Swal)

    constructor(props) {
        super(props);
        this.state = {password: '',username:''};
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
      }

      formateado=()=> {
        return {
            "password":this.state.password,
            "usuario":this.state.username,
            "name":'login'
        }
    }

    handleChangePassword(event){
        this.setState({password: event.target.value});
    }

    handleChangeUsername(event){
        this.setState({username: event.target.value});
    }

    //funcion para ir a dashboard
    login() {
        this.MySwal.fire({
            title: 'Confirmar Datos',
            html:<h1>¿Son tus credenciales?</h1>,
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
              return ApiService.add(this.formateado(),'login')
                .then(response => {
                    Swal.fire(
                        'Hecho!',
                        'Bienvenido!',
                        'success')
                        console.log(response);
                        localStorage.setItem("userID",response.data.id);
                        localStorage.setItem("admin",response.data.admin ? 1 : 2);
                        localStorage.setItem("password",response.data.password);
                        localStorage.setItem("name",response.data.name);
                        localStorage.setItem("usuario",response.data.usuario);
                    setTimeout(() => {
                        this.props.history.push('/dashboard/inicio');
                    }, 1000);
                })
                .catch(error => {
                    Swal.fire(
                      'Error!',
                      'Error no son tus credenciales',
                      'warning')
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
          })
    }


    componentDidMount(){
        if (localStorage.getItem("userID") && parseInt(localStorage.getItem("userID")) > 0 ) {
            
            this.props.history.push('/dashboard/inicio');
        }
      }

    render() {
        const { classes } = this.props;
        return (
            <Grid
  container
  spacing={0}
  direction="column"
  alignItems="center"
  justify="center"
  style={{ minHeight: '100vh' }}
>
<Grid item xs={3}>
                <Paper className={classes.padding}>
                <div className={classes.margin}>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Face />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField id="usuario" label="usuario" value={this.state.username} onChange={this.handleChangeUsername}  type="text" fullWidth autoFocus required />
                        
                        </Grid>
                    </Grid>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Fingerprint />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField id="password" label="Contraseña" value={this.state.password} onChange={this.handleChangePassword} type="password" fullWidth required />
                        </Grid>
                    </Grid>
                    <Grid container alignItems="center" justify="space-between">
                    </Grid>
                    <Grid container justify="center" style={{ marginTop: '10px' }}>
                        <Button variant="outlined" color="primary" style={{ textTransform: "none" }}
                        onClick={()=>this.login()}
                        >Iniciar sesion</Button>
                    </Grid>
                </div>
            </Paper>
            
            </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(LoginTab);