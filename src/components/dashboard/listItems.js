import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import HealingIcon from '@material-ui/icons/Healing';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import PersonIcon from '@material-ui/icons/Person';
import {withRouter} from "react-router-dom";
import List from '@material-ui/core/List';


class MainList extends React.Component {
  irAusuarios(){
    this.props.history.push('/dashboard/users');
  }
  irAproductos(){
    this.props.history.push('/dashboard/productos');
  } 
  irAyo(){
    this.props.history.push('/dashboard/yo');
  }
  irAinicio(){
    this.props.history.push('/dashboard/inicio');
  }
  irAventas(){
    this.props.history.push('/dashboard/ventas');
  }

  render(){
  return(
    <List>
    <ListItem button>
      <ListItemIcon>
        <DashboardIcon onClick={()=>{this.irAinicio()}} />
      </ListItemIcon>
      <ListItemText primary="Inicial" onClick={()=>{this.irAinicio()}} />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <ShoppingCartIcon onClick={()=>{this.irAventas()}} />
      </ListItemIcon>
      <ListItemText primary="Ventas" onClick={()=>{this.irAventas()}} />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <PeopleIcon onClick={()=>{this.irAusuarios()}} />
      </ListItemIcon>
      <ListItemText primary="Usuarios" onClick={()=>{this.irAusuarios()}} />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <HealingIcon onClick={()=>{this.irAproductos()}} />
      </ListItemIcon>
      <ListItemText primary="Medicina" onClick={()=>{this.irAproductos()}}/>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <PersonIcon onClick={()=>{this.irAyo()}}/>
      </ListItemIcon>
      <ListItemText primary="Yo"onClick={()=>{this.irAyo()}}/>
    </ListItem>
  </List>)
  };
}

export default withRouter(MainList);