import React from "react";
import { Container,Grid,Paper,makeStyles } from "@material-ui/core";
import { Alert, AlertTitle } from '@material-ui/lab';
import TableComponent from "../sharedComponents/tableComponent";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

export default function MostrarVentaComponent(props){
    const classes = useStyles();
    const columns =  [
      
      { title: 'Nombre', field: 'name' ,editable:"never"},
        { title: 'Vendedor', field: 'precio',editable:"never" },
        { title: 'cantidad', field: 'cantidad',editable:"never"},
      ];

      const data = [
        { Id: '1', name: 'Baran', precio: 1987, cantidad: 63 },
      ];
return (
    <Container className={classes.root}>
        <Grid container spacing={3} >
            <Grid item xs={12}>
            <Alert severity="info">
            <AlertTitle><strong>ATENCION!</strong></AlertTitle>
            <strong>No puede editar esta venta!</strong>
            </Alert>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <TableComponent title={"venta #"+props.location.state.idVenta} columns={columns} idBuscar={'/'+props.location.state.idVenta} data={data} modulo="ventas" />
                </Paper>
            </Grid>
        </Grid>
    </Container>
)

}