import React from "react";
import { Container,Grid,Paper,makeStyles } from "@material-ui/core";
import TableProdVent from "../sharedComponents/tableProdVent";
import { withRouter } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

function AgregarVentaComponent(props){
    const classes = useStyles();
    const columns =  [
      { title: 'Id', field: 'id',editable:"never"},
      { title: 'Nombre', field: 'name' },
      { title: 'Precio', field: 'precio', type: 'numeric' },
      {
        title: 'Descripcion',
        field: 'descripcion',
        //lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
      },
    ];

return (
    <Container className={classes.root}>
        <Grid container spacing={3} >
            <Grid item xs={12}>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <TableProdVent columns={columns} title="Venta de Productos" />
                </Paper>
            </Grid>
        </Grid>
    </Container>
)

}

export default withRouter(AgregarVentaComponent);