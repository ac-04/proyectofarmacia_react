import React from "react";
import { Container,Grid,Paper,makeStyles } from "@material-ui/core";
import CheckIcon from '@material-ui/icons/Check';
import EditIcon from '@material-ui/icons/Edit';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { Alert, AlertTitle } from '@material-ui/lab';
import TableComponent from "../sharedComponents/tableComponent";
import { withRouter } from "react-router-dom";
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

function VentasComponent(props){
    const classes = useStyles();
    const columns =  [
        { title: 'Id', field: 'id',editable:"never"},
        { title: 'fecha creación', field: 'createdAt' ,editable:"never"},
        { title: 'Vendedor', field: 'nombre',editable:"never" }
      ];

      const data = [
        { Id: '1', name: 'Baran', precio: 1987, cantidad: 63 },
      ];
return (
    <Container className={classes.root}>
        <Grid container spacing={3} >
            <Grid item xs={12}>
            <Alert severity="info">
            <AlertTitle><strong>AGREGAR UNA VENTA!</strong></AlertTitle>
            Para ver una venta presione <AddBoxIcon/> y despues para confirmar  <CheckIcon/> — <strong>Pruebalo!</strong>
            <div>o ir agregar una venta <Button variant="contained" onClick={()=>props.history.push('/dashboard/agregar/venta')} color="secondary">Agregar Venta</Button></div>
            </Alert>
            <br/>
            <Alert severity="warning">
            <AlertTitle><strong>ATENCION!</strong></AlertTitle>
            Para ver una venta presione <EditIcon/> y despues para confirmar  <CheckIcon/> — <strong>Pruebalo!</strong>
            </Alert>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <TableComponent title="ventas" columns={columns} data={data} modulo="ventas" />
                </Paper>
            </Grid>
        </Grid>
    </Container>
)

}

export default withRouter(VentasComponent);