import React, { useEffect } from "react";
import { Container,Grid,Paper,makeStyles } from "@material-ui/core";


import { withRouter } from 'react-router-dom';

import TableComponent from "../sharedComponents/tableComponent";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

function UsersComponent(props){


  useEffect(() => {
    if (localStorage.getItem("admin") && parseInt(localStorage.getItem("admin")) === 1 ) {
  }else{
    props.history.push('/');
  }
  }, [props])

    const classes = useStyles();
    const columns =  [
        { title: 'Id', field: 'id',editable:"never"},
        { title: 'Nombre', field: 'name',editable:"onAdd" },
        { title: 'Usuario', field: 'usuario',editable:"onAdd" },
        {
          title: 'Tipo Acceso',
          field: 'admin',
          lookup: { true: 'Administrador', false: 'Vendedor' },
        },
        {
            title: 'cambiar Contraseña',
            field: 'password',
            lookup: { 'cambiar': 'Resetear', 'no': 'No hacer nada' },
        },
      ];
      const data = [
        { Id: '1', name: 'Baran', usuario: 'elnegris', isAdmin: true },
      ];
return (
    <Container className={classes.root}>
        <Grid container spacing={3} >
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <TableComponent title="usuarios" columns={columns} data={data} modulo="usuarios" />
                </Paper>
            </Grid>
        </Grid>
    </Container>
)

}

export default withRouter(UsersComponent);