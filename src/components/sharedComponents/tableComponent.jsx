import React, {useEffect} from 'react';
import MaterialTable from 'material-table';
import ApiService from "../../config/services/apiService";
import { withRouter } from 'react-router-dom';

import Swal from 'sweetalert2'

function TableComponent(props) {

    const [state, setState] = React.useState({
        columns:props.columns,
        data: [],
        modulo: props.modulo
    });

    useEffect(() => { 
      let modulo = props.modulo;
      if (props.modulo === "ventas") {
        if (props.idBuscar) {
          
        }else{
          console.log(typeof(props.idBuscar),props.idBuscar);
            modulo = "ventas/user/"+localStorage.getItem("userID");
        }
      }
      let url = modulo+(props.idBuscar ? props.idBuscar : '');
        ApiService.fetchObjects(url).then((res)=>{
            setState({data:res.data})
        });  
        return () => console.log('unmount finished')
    },[props]);

  return (
    <MaterialTable
      title={props.title}
      columns={props.columns}
      data={state.data}
      editable={{
        //isEditable: rowData => props.modulo !=="ventas", // si es ventas no se editara
        onRowAdd:  (newData) =>
        new Promise((resolve) => {
          if(props.modulo !== "ventas"){
            ApiService.add(newData,props.modulo).then((res)=>{
              resolve();
                setState((prevState) => {
                  const data = [...prevState.data];
                  data.push(res.data);
                  return { ...prevState, data };
                })
              })
          }else{
            
          props.history.push({
            pathname: '/dashboard/agregarventa'
          })
            resolve();
            setState((prevState) => {
              const data = [...prevState.data];
              return { ...prevState, data };
            });
          }
        }),
        onRowUpdate:  (newData, oldData) =>
        new Promise((resolve) => {
         if(props.modulo!=="ventas"){
          ApiService.edit(newData,props.modulo).then((res)=>{
            resolve();
            if (oldData) {
              setState((prevState) => {
                const data = [...prevState.data];
                data[data.indexOf(oldData)] = res.data;
                return { ...prevState, data };
              });
            }
          })
         }else{
          props.history.push({
            pathname: '/dashboard/mostrarventa',
            search: '?idVenta='+oldData.id,
            state: { idVenta:oldData.id}
          })
          resolve();
          setState((prevState) => {
            const data = [...prevState.data];
            return { ...prevState, data };
          });
         }
        }),
        onRowDelete: (oldData) =>
          new Promise((resolve,error) => {
            ApiService.delete(oldData.id,props.modulo+"/").then((res)=>{
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }).catch((err)=>{
              error();
              Swal.fire(
                'Error!',
                'Este producto no se puede borrar debido a que se encuentra en una venta',
                'warning')
                props.history.push("/dashboard/productos");
                setState((prevState) => {
                  const data = [...prevState.data];
                  return { ...prevState, data };
                });
            });;
          }),
      }}
      options={{
        addRowPosition: 'first', // esto hace que la primera columna sirva para poder agregar un nuevo objeto
        exportButton: true,
        
      selection: true
      }}
      localization={{
        body: {
          emptyDataSourceMessage: 'No se encuentran registros',
          addTooltip: 'Agregar',
          deleteTooltip: 'Eliminar',
          editRow: {
            deleteText: 'Desea eliminar este registro?',
            cancelTooltip: 'Cancelar',
            saveTooltip: 'Agregar'
          }
        },
        toolbar: {
          searchTooltip: 'Buscar',
        },
        pagination: {
          labelRowsSelect: 'lineas',
          labelDisplayedRows: '{count} de {from}-{to}',
          firstTooltip: 'Primera página',
          previousTooltip: 'Página anterior',
          nextTooltip: 'Próxima página',
          lastTooltip: 'Última página'
        }
      }}
      components={props.actions}
    />
  );
}

export default withRouter(TableComponent)