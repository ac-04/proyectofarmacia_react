import React, {useEffect} from 'react';
import MaterialTable from 'material-table';
import ApiService from "../../config/services/apiService";
import { withRouter } from 'react-router-dom';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import SendIcon from '@material-ui/icons/Send';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import { Grid,Paper,makeStyles } from "@material-ui/core";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme)=>({
  table: {
    minWidth: 650,
  },
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));
const MySwal = withReactContent(Swal)
function TableProdVent(props) {
  const classes = useStyles();
    const [state, setState] = React.useState({
        columns:props.columns,
        data: [],
        copyData:[],
        modulo: props.modulo,
        bag:[]
    });

    function formateado(){
       state.bag.forEach((prod)=>{
        delete prod.tableData
      })
      return state.bag;
    }

    function renderList(){
      MySwal.fire({
        title: 'Confirmar venta',
        html:<div className={classes.root}>
         <List component="nav" aria-label="secondary mailbox folders">
           {state.bag.map((prod)=>(
            <ListItem>
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary={prod.name+'  cantidad:'+prod.cantidad+'  total:$'+((prod.cantidad*prod.precio).toFixed(2))} />
            </ListItem>
           ))}

        </List>
          </div>,
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
          return ApiService.add(formateado(),'ventas/'+localStorage.getItem("userID"))
            .then(response => {
              if (!response.ok) {
                throw new Error("error desde aqui")
              }
              return response.json()
            })
            .catch(error => {
                Swal.fire(
                  'Hecho!',
                  'Venta completada, redireccionando a ventas',
                  'success')
                  
            setTimeout(() => {
              props.history.push("/dashboard/ventas");
            }, 1000);
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Hecho!',
            'Venta completada, redireccionando a ventas',
            'success')
        }
        setTimeout(() => {
          props.history.push("/dashboard/ventas");
        }, 1000);
      })
    }

    
    function setBag(data){
      data[data.length-1].cantidad = -1;
      MySwal.fire({
        title: 'Cantidad de producto',
        input: 'number',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: false,
        confirmButtonText: 'Agregar',
        showLoaderOnConfirm: true,
        preConfirm: (CantidadIngresada) => {
          data[data.length-1].cantidad = CantidadIngresada>0 ? CantidadIngresada : 1;
          return CantidadIngresada;
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(() => {
          data[data.length-1].cantidad = data[data.length-1].cantidad<0 ? 1 : data[data.length-1].cantidad;
        setState({data:localStorage.getItem("productos"),bag:data})
      return MySwal.fire(<p>Agregado!</p>)
      }).catch(error => {
        Swal.showValidationMessage(
         "Ingrese un numero por favor"
        )
      })
    }
    
    function quitardebag(data){
      setState({data:localStorage.getItem("productos"),bag:data})
    }
    function total(){
        let total = 0.0;
        state.bag.forEach((prod)=>{
            total+=prod.precio*prod.cantidad;
        });
        return total.toFixed(2);
    }
    useEffect(() => {   
        ApiService.fetchObjects('productos').then((res)=>{
            setState({data:res.data,bag:[]})
            localStorage.setItem("productos",res.data)
        });  
        return () => console.log('unmount finished')
    },[props]);

  return (
      <div>
        
        <Grid container spacing={3} >
        <Grid item xs={12}>
           <h1>productos #:{state.bag.length} total: $ {state.bag.length > 0 ? total(): '0' }</h1>
           <br/>
           <Button disabled={state.bag.length===0} variant="contained" onClick={()=>renderList()} color="primary">Realizar venta</Button>
           </Grid>
           
           <Grid item xs={6}>
    <MaterialTable
      title={props.title}
      columns={props.columns}
      data={state.data}
      options={{
        addRowPosition: 'first', // esto hace que la primera columna sirva para poder agregar un nuevo objeto
        exportButton: true,
        search: false,
      selection: true,
      showSelectAllCheckbox:false
      }}
      localization={{
        body: {
          emptyDataSourceMessage: 'No se encuentran registros',
          addTooltip: 'Agregar',
          deleteTooltip: 'Eliminar',
          editRow: {
            deleteText: 'Desea eliminar este registro?',
            cancelTooltip: 'Cancelar',
            saveTooltip: 'Agregar'
          }
        },
        toolbar: {
          searchTooltip: 'Buscar',
        },
        pagination: {
          labelRowsSelect: 'lineas',
          labelDisplayedRows: '{count} de {from}-{to}',
          firstTooltip: 'Primera página',
          previousTooltip: 'Página anterior',
          nextTooltip: 'Próxima página',
          lastTooltip: 'Última página'
        }
      }}
      components={
          {
              Actions:()=>null
          }
      }
      actions={[
        {
          tooltip: 'Remove All Selected Users',
          icon: 'delete',
          onClick: (evt, data) => alert('You want to delete ' + data.length + ' rows')
        }
      ]}
      onSelectionChange={(rows,current) => state.bag.findIndex(prod=>prod.id===current.id) > -1 ? quitardebag(rows) :setBag(rows)}
    />
</Grid>
<Grid item xs={6}>
<h2 class="text-center">Esta venta</h2>
<TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell align="right">Precio</TableCell>
            <TableCell align="right">cantidad</TableCell>
            <TableCell align="right">Total</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {state.bag.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.precio}</TableCell>
              <TableCell align="right">{row.cantidad}</TableCell>
              <TableCell align="right">{(row.cantidad*row.precio).toFixed(2)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </Grid>
</Grid>
    </div>
  );
}

export default withRouter(TableProdVent)