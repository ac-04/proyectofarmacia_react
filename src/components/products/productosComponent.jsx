import React from "react";
import { Container,Grid,Paper,makeStyles } from "@material-ui/core";

import TableComponent from "../sharedComponents/tableComponent";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

export default function ProductosComponent(props){
    const classes = useStyles();
    const columns =  [
        { title: 'Id', field: 'id',editable:"never"},
        { title: 'Nombre', field: 'name' },
        { title: 'Precio', field: 'precio', type: 'numeric' },
        {
          title: 'Descripcion',
          field: 'descripcion',
          //lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
        },
      ];
      const data = [
        { Id: '1', name: 'Baran', precio: 1987, cantidad: 63 },
      ];
return (
    <Container className={classes.root}>
        <Grid container spacing={3} >
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <TableComponent title="productos" columns={columns} data={data} modulo="productos" />
                </Paper>
            </Grid>
        </Grid>
    </Container>
)

}