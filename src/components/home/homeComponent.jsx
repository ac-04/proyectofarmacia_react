import React from "react";

import { Container,Grid,makeStyles,Typography,Button,CardContent,CardActions,Card   } from "@material-ui/core";
import { withRouter } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    minWidth: 275,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 30,
  },
  pos: {
    marginBottom: 12,
  },
}));

function HomeComponent(props){
  const classes = useStyles();

  return (
    <Container className={classes.root}>
    <Grid container spacing={3} >
        <Grid item xs={12}>
          <Typography variant="h1" align="center" component="h2">
          FARMACIA
          </Typography>
        </Grid>
        <Grid item xs={6}>
        <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          VENTAS
        </Typography>
        <Typography variant="h5" component="h2">
          ventas realizadas 
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" variant="contained" onClick={()=>{props.history.push("/dashboard/ventas")}} color="secondary" >IR A VENTAS</Button>
      </CardActions>
    </Card>
        </Grid>        
        <Grid item xs={6}>
        <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          PRODUCTOS
        </Typography>
        <Typography variant="h5" component="h2">
          Productos registrados 
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" variant="contained" onClick={()=>{props.history.push("/dashboard/productos")}} color="primary" >IR A PRODUCTOS</Button>
      </CardActions>
    </Card>
        </Grid>
      </Grid>
    </Container>
  );

}

export default withRouter(HomeComponent);