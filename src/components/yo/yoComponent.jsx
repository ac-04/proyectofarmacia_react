/* eslint-disable react-hooks/exhaustive-deps */
import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {Container,Typography,Button} from "@material-ui/core";
import ApiService from "../../config/services/apiService";

import withReactContent from 'sweetalert2-react-content'
import Swal from 'sweetalert2'
const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function YoComponent(props) {
    
    const MySwal = withReactContent(Swal)
    const classes = useStyles();
    var [nombre, setNombre] = useState(0);
    var [password, setPassword] = useState(0);
    var [username, setUsername] = useState(0);


    function formateado(){
        return {
            "usuario":username,
            "name":nombre,
            "password":password,
            "id":localStorage.getItem("userID")
        }
    }


    function updateUser(){
        MySwal.fire({
            title: 'Confirmar Datos',
            html:<h1>¿Son tus credenciales correctas?</h1>,
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
              return ApiService.edit(formateado(),'usuarios/unico')
                .then(response => {
                    Swal.fire(
                        'Hecho!',
                        'Datos Actualizado!',
                        'success')
                })
                .catch(error => {
                    Swal.fire(
                      'Error!',
                      'Error',
                      'warning')
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
          })
    }

 

  useEffect(() => {
      setNombre(nombre=localStorage.getItem("name"))
      setPassword(password=localStorage.getItem("password"))
      setUsername(username=localStorage.getItem("usuario"))
  }, [props])

  return (
    <Container className={classes.root}>
    <Grid container spacing={3} >
        <Grid item xs={12}>
          <Typography variant="h5" align="center" component="h2">
          USUARIO <br></br>{localStorage.getItem("name")}
          </Typography>
        </Grid>
        <Grid item xs={12}>
        <Typography variant="h5" align="center" component="h2">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="input-with-icon-adornment">Nombre</InputLabel>
        <Input
        value={nombre}
        error={nombre.length < 1}
        onChange={ (event)=>{setNombre(nombre=event.target.value);}}
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          }
        />
      </FormControl>
      </Typography>
        </Grid>
        <Grid item xs={12}>
        <Typography variant="h5" align="center" component="h2">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="input-with-icon-adornment">Usuario</InputLabel>
        <Input
        value={username}
        error={username ? username.length < 1 : true}
        onChange={ (event)=>{setUsername(username=event.target.value);}}
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          }
        />
      </FormControl>
      </Typography>
        </Grid>

        <Grid item xs={12}>
        <Typography variant="h5" align="center" component="h2">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="input-with-icon-adornment">Password</InputLabel>
        <Input
        value={password}
        error={password ? password.length < 1 : true}
        onChange={ (event)=>{setPassword(password=event.target.value);}}
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <AccountCircle />
            </InputAdornment>
          }
        />
      </FormControl>
      </Typography>
        </Grid>
        <Grid item xs={12}>
        <Typography variant="h5" align="center" component="h2">
        <Button size="small" variant="contained" onClick={updateUser} color="primary" >ACTUALIZAR DATOS</Button>
      
        </Typography>
          </Grid>
      </Grid>
    </Container>
  );
}
