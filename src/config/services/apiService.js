import axios from 'axios';

const USER_API_BASE_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/' : 'https://secure-taiga-63185.herokuapp.com/';

class ApiService {

    fetchObjects(module) {
        return axios.get(USER_API_BASE_URL+module);
    }

    fetchObjectById(id,module) {
        return axios.get(USER_API_BASE_URL +module + id);
    }

    delete(id,module) {
        return axios.delete(USER_API_BASE_URL + module + id);
    }

    add(objeto,module) {
        return axios.post(""+USER_API_BASE_URL+module, objeto);
    }

    edit(objeto,module) {
        return axios.put(USER_API_BASE_URL + module, objeto);
    }

}

export default new ApiService();