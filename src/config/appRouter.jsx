import { BrowserRouter as Router, Route,Switch } from "react-router-dom";
import LoginTab from "../components/login/login";
import Dashboard from "../components/dashboard/Dashboard";
import React from "react";
import UsersComponent from "../components/users/ListUserComponent";
import HomeComponent from "../components/home/homeComponent";
import ProductosComponent from "../components/products/productosComponent";
import VentasComponent from "../components/ventas/ventasComponent";
import MostrarVentaComponent from "../components/ventas/mostrarVentaComponent";
import AgregarVentaComponent from "../components/ventas/agregarVentaComponent";
import YoComponent from "../components/yo/yoComponent";
const AppRouter = () =>{
    return (
            <Router>
                <Switch>
                    <Route path="/" exact component={LoginTab}></Route>
                    <Dashboard>
                    <Route path="/dashboard" component={({ match }) =>
                    <div>
                        <Route path='/dashboard/inicio' component={HomeComponent} />
                        <Route path='/dashboard/users' component={UsersComponent} />
                        <Route path='/dashboard/productos' component={ProductosComponent} />
                        <Route path='/dashboard/ventas' component={VentasComponent} />
                        <Route path='/dashboard/mostrarventa' component={MostrarVentaComponent} />
                        <Route path='/dashboard/agregar/venta' component={AgregarVentaComponent} />
                        <Route path='/dashboard/yo' component={YoComponent} />
                    </div>
                    }/>
                    </Dashboard>
                </Switch>
            </Router>
    )
}


export default AppRouter;